### EKS Cluster creation using eksctl command

#### Pre-requsites
* AWS Account with Admin access

#### Softwares required
* AWS CLI 2
* Kubectl
* Eksctl

![alt text](eksctl.jpg)

#### Steps
* Create an admin user in AWS IAM.
* Create an EC2 instance. By default it will launch in Default VPC.
* Install above softwares.
* Run ```aws configure``` to authenticate.
* Create eksctl-config.yaml.
* Run below command to create cluster.
```
eksctl create cluster -f <file-name>
```
* It will take around 20min to create cluster. Run below command to know the cluster is ready.
```
kubectl get nodes
```
* kubernetes config file will be create in ~/.kube folder with name config. Copy this config file to any system to connect to cluster.<br/>

**NOTE:** The above procedure can be run from Windows Laptop as well.

#### Connect from Windows Laptop.
* kubectl and AWS CLI2 should be installed in the laptop.
* Run ```aws configure``` command.
* Copy the config file to windows latop to ~/.kube directory .
* set the below environment variable in cmd prompt.
```
set KUBECONFIG=<path-to-config-file>
```
* Run kubectl get nodes to show the cluster nodes information<br/>

**NOTE:** Don't forget to delete the cluster after your practice.
```
eksctl delete cluster -f <file-name>
```